import board
import digitalio
import adafruit_dht
import time
import adafruit_sdcard
import analogio
import busio
import storage
import adafruit_ssd1306
import gc

gc.collect()

dht = adafruit_dht.DHT11(board.D5)

vbat_voltage = analogio.AnalogIn(board.D9)
lt = time.localtime()

i2c = busio.I2C(board.SCL, board.SDA)

oled = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c)

oled.fill(0)
oled.text("{}/{}/{} {}:{}:{}".format(lt.tm_mon, lt.tm_mday, lt.tm_year, lt.tm_hour, lt.tm_min, lt.tm_sec), 0, 0)
oled.text('Pretty', 0, 10)
oled.text('Titties', 0, 20)
oled.show()

SD_CS = board.D10
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
cs = digitalio.DigitalInOut(SD_CS)
sd_card = adafruit_sdcard.SDCard(spi, cs)
vfs = storage.VfsFat(sd_card)
storage.mount(vfs, "/sd_card")

def convert_c_to_f(c):
    return (c * 9/5) + 32


def get_voltage(pin):
    return (pin.value * 3.3) / 65536 * 2


def update_display(text_tuples):
    oled.fill(0)
    for text, x, y in text_tuples:
        oled.text(text, x, y)
    oled.show()


print("Logging temperature and humidity to log file")

initial_time = time.monotonic()

while True:
    try:
        with open("/sd_card/log.txt", "a") as sdc:
            try:
                temperature = convert_c_to_f(dht.temperature)
                humidity = dht.humidity
            except RuntimeError:
                temperature = 'ERROR'
                humidity = 'ERROR'
            battery_voltage = get_voltage(vbat_voltage)
            current_time = time.monotonic()
            time_stamp = current_time - initial_time
            print("Seconds since current data log started:", int(time_stamp))
            print("Temperature:", temperature)
            print("Humidity:", humidity)
            print("VBat voltage: {:.2f}".format(battery_voltage))
            print()
            lt = time.localtime()
            update_display([
                ("{}/{}/{} {}:{}:{}".format(lt.tm_mon, lt.tm_mday, lt.tm_year, lt.tm_hour, lt.tm_min, lt.tm_sec), 0, 0),
                ('Temperature: {}'.format(temperature), 0, 10),
                ('Humidity: {}'.format(humidity), 0, 20)
            ])
            sdc.write("{}, {}, {}, {:.2f}\n".format(
                int(time_stamp), temperature,
                humidity, battery_voltage)
                     )
        time.sleep(10)
    except OSError:
        pass
    except RuntimeError:
        pass
