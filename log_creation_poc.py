#! python3.6

import time
import random

OLED_DATE_FMT = "{month}/{day} {hour}:{min:0>2}:{sec:0>2}"
FULL_DATE_FMT = "{month}/{day}/{year} {hour}:{min:0>2}:{sec:0>2}"
FILENAME_DATE_FMT = "{year}-{month:0>2}-{day:0>2}"
EXTENDED_MONTHS = [1, 3, 5, 7, 8, 10, 12]


LOOP_TIMEOUT = 10


def make_timestamp(time_struct=None, format_string=FULL_DATE_FMT):
    time_struct = time_struct or time.localtime()
    time_dict = {
        'month': time_struct.tm_mon,
        'day': time_struct.tm_mday,
        'year': time_struct.tm_year,
        'hour': time_struct.tm_hour,
        'min': time_struct.tm_min,
        'sec': time_struct.tm_sec
    }
    timestamp = format_string.format(**time_dict)

    return timestamp


def get_temperature():
    return random.sample(range(64, 80), 1)[0] + random.random()


def get_humidity():
    return random.sample(range(61, 98), 1)[0] + random.random()


def resolve_filename(date=None):
    date = date or time.localtime()
    filename = 'log_{}.txt'.format(
        make_timestamp(time_struct=date, format_string=FILENAME_DATE_FMT)
    )
    return filename


def filter_file(filename, filter_stmt):
    #print('looking for logs for {} in {}'.format(filter_stmt, filename))
    with open(filename) as f:
        lines = f.readlines()
        # l = list(filter(lambda x: '11/6/2018 12:00' in  x, lines))
        l = list(filter(lambda x: filter_stmt in  x, lines))
    return l


def previous_log():
    global log_date
    global log_hour
    try:
        log_date
    except NameError:
        log_date = time.localtime()

    try:
        log_hour
    except NameError:
        print('resolving time')
        log_hour = int(make_timestamp(log_date, format_string='{hour}'))

    while log_hour > 0:
        hour_string = ' {}:00'.format(log_hour)
        filename = resolve_filename(log_date)
        try:
            log_entries = filter_file(filename, hour_string)
        except FileNotFoundError:
            print('Unable to locate file: {}'.format(filename))
            log_date = previous_day(log_date)
            return previous_log()

        log_hour -= 1
        if log_entries:
            return log_entries[0].strip('\n')
    else:
        print('Unable to find previous log entries')
        log_date = previous_day(log_date)
        return previous_log()


def previous_day(date):
    global log_hour
    tm_mday = date.tm_mday
    if tm_mday > 1:
        tm_mday -= 1
    else:
        return previous_month(date)

    log_hour = 23
    
    date = default_time_struct(tm_mday=tm_mday, time_struct=date)
    
    return date


def previous_month(date):
    tm_mon = date.tm_mon
    if tm_mon > 1:
        tm_mon -= 1
        if tm_mon in EXTENDED_MONTHS:
            tm_mday = 31
        elif tm_mon == 2:
            tm_mday = 28
        else:
            tm_mday = 30
    else:
        return previous_year(date)

    date = default_time_struct(
        tm_mon=tm_mon,
        tm_mday=tm_mday,
        time_struct=date
    )

    return date


def previous_year(date):
    tm_year = date.tm_year - 1
    date = default_time_struct(
        tm_year=tm_year,
        tm_mon=12, 
        tm_mday=31,
        time_struct=date
    )

    return date


def default_time_struct(
    tm_year=None, 
    tm_mon=None, 
    tm_mday=None, 
    tm_hour=None, 
    tm_min=None,
    time_struct=None
):
    date = time_struct or time.localtime()
    return time.struct_time((
        tm_year or date.tm_year,
        tm_mon or date.tm_mon,
        tm_mday or date.tm_mday,
        tm_hour or date.tm_hour,
        tm_min or date.tm_min,
        0, 0, 0, 0
    ))


def default_time_dict(**kwargs):
    time_struct = time.localtime()
    time_dict = {
        'month': time_struct.tm_mon,
        'day': time_struct.tm_mday,
        'year': time_struct.tm_year,
        'hour': time_struct.tm_hour,
        'min': time_struct.tm_min,
        'sec': time_struct.tm_sec
    }
    time_dict.update(kwargs)

    return time_dict

previous_time = time.time()


def main_loop():
    timestamp = make_timestamp()
    filename = resolve_filename()

    try:
        temperature = get_temperature()
        humidity = get_humidity()
    except (RuntimeError, TypeError):
        temperature = 'ERROR'
        humidity = 'ERROR'
    print(timestamp, "|", "Temperature:", '{:.2f}'.format(temperature), 
    "|", "Humidity:", '{:.1f}'.format(humidity))
    with open("./%s" % filename, "a") as sdc:
        sdc.write("{},{:.2f},{:.1f}%\n".format(
            timestamp, temperature, humidity)
        )


if __name__ == '__main__':
    print("Logging temperature and humidity to log file")
    while True:
        current_time = time.time()
        if current_time - previous_time >= LOOP_TIMEOUT:
            main_loop()
            previous_time = current_time
