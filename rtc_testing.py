# import busio
# import board
import time
# import rtc
# from adafruit_pcf8523 import PCF8523

# i2c_bus = busio.I2C(board.SCL, board.SDA)
# rt_clock = PCF8523(i2c_bus)
# rtc.set_time_source(rt_clock)
from adalogger_rtc import rt_clock
lt = rt_clock.datetime
print("{}/{}/{} {}:{}:{}".format(lt.tm_mon, lt.tm_mday, lt.tm_year, lt.tm_hour, lt.tm_min, lt.tm_sec))

def clock():
    while True:
        lt = time.localtime()
        print("{}/{}/{} {}:{:0>2d}:{:0>2d}".format(
            lt.tm_mon, lt.tm_mday, lt.tm_year, lt.tm_hour, lt.tm_min, lt.tm_sec), end='\r')
        time.sleep(1)