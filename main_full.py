import gc
from adalogger_rtc import i2c_bus as i2c
import board
import digitalio
import adafruit_dht
import time
import adafruit_sdcard
import analogio
import busio
import storage
import adafruit_ssd1306
import gamepad

gc.collect()

OLED_DATE_FMT = "{month}/{day} {hour}:{min:0>2}:{sec:0>2}"
FULL_DATE_FMT = "{month}/{day}/{year} {hour}:{min:0>2}:{sec:0>2}"
FILENAME_DATE_FMT = "{year}-{month:0>2}-{day:0>2}"
EXTENDED_MONTHS = [1, 3, 5, 7, 8, 10, 12]

BUTTON_A = 1 << 0
BUTTON_B = 1 << 1
BUTTON_C = 1 << 2

# TODO: Split timeout so OLED refresh can be held
# without impacting logging
LOOP_TIMEOUT = 10
REFRESH_TIMEOUT = 60

dht = adafruit_dht.DHT11(board.D11)
pad = gamepad.GamePad(
    digitalio.DigitalInOut(board.D9),
    digitalio.DigitalInOut(board.D6),
    digitalio.DigitalInOut(board.D5)
)

SD_CS = board.D10
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
cs = digitalio.DigitalInOut(SD_CS)
sd_card = adafruit_sdcard.SDCard(spi, cs)

vfs = storage.VfsFat(sd_card)
storage.mount(vfs, "/sd_card")


def convert_c_to_f(c):
    return (c * 9/5) + 32


def get_voltage(pin):
    return (pin.value * 3.3) / 65536 * 2


def resolve_filename(date=None):
    date = date or time.localtime()
    filename = 'log_{}.txt'.format(
        make_timestamp(time_struct=date, format_string=FILENAME_DATE_FMT)
    )
    return filename


def filter_file(filename, filter_stmt):
    with open(filename) as f:
        for line in f:
            if filter_stmt in line:
                return line
    return log_entries


def next_log():
    update_display([
        ('next_log', 0, 0),
        ('Not Implemented Yet!', 0, 10)
    ])


def display_now(temperature, humidity):
    global log_date
    global log_hour
    try:
        del log_date
        del log_hour
    except KeyError:
        pass

    timestamp = make_timestamp()
    oled_timestamp = make_timestamp(format_string=OLED_DATE_FMT)
    update_display([
        (oled_timestamp, 0, 0),
        ('Temp: {:.2f}'.format(temperature), 0, 10),
        ('Humidity: {:.1f}%'.format(humidity), 0, 20)
    ])



def display_previous_log():
    update_display([
        ('Loading...', 0, 10)
    ])
    timestamp, temperature, humidity = previous_log().split(',')
    update_display([
        (timestamp, 0, 0),
        ('Temp: {}'.format(temperature), 0, 10),
        ('Humidity: {}'.format(humidity), 0, 20)
    ])



def previous_log():
    global log_date
    global log_hour
    try:
        log_date
    except NameError:
        log_date = time.localtime()

    try:
        log_hour
    except NameError:
        print('resolving time')
        log_hour = int(make_timestamp(log_date, format_string='{hour}'))

    while log_hour >= 0:
        hour_string = ' {}:00'.format(log_hour)
        filename = '/sd_card/%s' % resolve_filename(log_date)
        try:
            log_entries = filter_file(filename, hour_string)
        except OSError:
            print('Unable to locate file: {}'.format(filename))
            log_date = previous_day(log_date)
            return previous_log()

        log_hour -= 1
        if log_entries:
            return log_entries.strip('\n')
    else:
        print('Unable to find previous log entries')
        log_date = previous_day(log_date)
        return previous_log()


def previous_day(date):
    global log_hour
    tm_mday = date.tm_mday
    if tm_mday > 1:
        tm_mday -= 1
    else:
        return previous_month(date)

    log_hour = 23

    date = default_time_struct(tm_mday=tm_mday, time_struct=date)

    return date


def previous_month(date):
    tm_mon = date.tm_mon
    if tm_mon > 1:
        tm_mon -= 1
        if tm_mon in EXTENDED_MONTHS:
            tm_mday = 31
        elif tm_mon == 2:
            tm_mday = 28
        else:
            tm_mday = 30
    else:
        return previous_year(date)

    date = default_time_struct(
        tm_mon=tm_mon,
        tm_mday=tm_mday,
        time_struct=date
    )

    return date


def previous_year(date):
    tm_year = date.tm_year - 1
    date = default_time_struct(
        tm_year=tm_year,
        tm_mon=12,
        tm_mday=31,
        time_struct=date
    )

    return date


def default_time_struct(
    tm_year=None,
    tm_mon=None,
    tm_mday=None,
    tm_hour=None,
    tm_min=None,
    time_struct=None
):
    date = time_struct or time.localtime()
    return time.struct_time((
        tm_year or date.tm_year,
        tm_mon or date.tm_mon,
        tm_mday or date.tm_mday,
        tm_hour or date.tm_hour,
        tm_min or date.tm_min,
        0, 0, 0, 0
    ))


def update_display(text_tuples, clear=True):
    if clear:
        oled.fill(0)
    for text, x, y in text_tuples:
        oled.text(text, x, y)
    oled.show()


def make_timestamp(time_struct=None, format_string=FULL_DATE_FMT):
    time_struct = time_struct or time.localtime()
    time_dict = {
        'month': time_struct.tm_mon,
        'day': time_struct.tm_mday,
        'year': time_struct.tm_year,
        'hour': time_struct.tm_hour,
        'min': time_struct.tm_min,
        'sec': time_struct.tm_sec
    }
    timestamp = format_string.format(**time_dict)

    return timestamp


print("Logging temperature and humidity to log file")

oled_timestamp = make_timestamp(format_string=OLED_DATE_FMT)

oled = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c)

oled.fill(0)
oled.text(oled_timestamp, 0, 0)
oled.text('Initializing...', 0, 10)
oled.show()

previous_time = time.time()
last_pressed = 0

def main_loop():
    timestamp = make_timestamp()
    filename = 'log_{}.txt'.format(
        make_timestamp(format_string=FILENAME_DATE_FMT)
    )

    try:
        temperature = convert_c_to_f(dht.temperature)
        humidity = dht.humidity
    except (RuntimeError, TypeError):
        temperature = 'ERROR'
        humidity = 'ERROR'
    print("Temperature:", temperature)
    print("Humidity:", humidity)
    print()

    with open("/sd_card/%s" % filename, "a") as sdc:
        sdc.write("{},{:.2f},{:.1f}%\n".format(
            timestamp, temperature, humidity)
        )
    return temperature, humidity

reset_display = False
while True:
    try:
        buttons = pad.get_pressed()
        if buttons & BUTTON_A:
            print('Pressed Button A')
            display_previous_log()
        elif buttons & BUTTON_B:
            print('Pressed Button B')
            reset_display = True
            update_display([
                ('Displaying Now...', 0, 10)
            ])
        elif buttons & BUTTON_C:
            print('Pressed Button C')
            next_log()
        time.sleep(0.1) # Debounce
        while buttons:
            # Wait for all buttons to be released.
            buttons = pad.get_pressed()
            last_pressed = time.time()
            time.sleep(0.1)

        current_time = time.time()
        if current_time - previous_time >= LOOP_TIMEOUT:
            temperature, humidity = main_loop()
            previous_time = current_time

            if reset_display or \
            current_time - last_pressed >= REFRESH_TIMEOUT:
                    display_now(temperature, humidity)
                    reset_display = False


    except OSError:
        print('OSError')
    except RuntimeError:
        print('RuntimeError')
