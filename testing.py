import gc

# 18.2K Free

lib_list = [
    'board', # 16B
    'digitalio', # 0B
    'adafruit_dht', # 2.6K | 2656B
    'time', # 16B
    'adafruit_sdcard', # 5.6K | 5680
    'analogio', # 0B
    'busio', # 16B
    'storage', # 0B
    #'adafruit_ssd1306', # 4.7K | 4832
    #'adafruit_pcf8523', # 8.9K | 9152B
]


def get_library_size(lib):
    gc.collect()
    mem_before = gc.mem_free()
    exec('import {}'.format(lib))
    gc.collect()
    mem_after = gc.mem_free()
    total_mem = mem_before - mem_after
    return total_mem

def test_libs(libs):
    gc.collect()
    print('Testing libraries | {}B Mem Free'.format(gc.mem_free()))
    for lib in libs:
        try:
            print('{}: {} bytes'.format(lib, get_library_size(lib)))
        except MemoryError:
            print('Not enough memory free' \
            ' to test {} | {}B Free'.format(lib, gc.mem_free()))
    gc.collect()
    print('{}B free'.format(gc.mem_free()))

test_libs(lib_list)

def unload(module):
    import sys
    del sys.modules[module]
    del globals()[module]
    print('Unloaded', module)


###############################
# >>> import testing
# Testing libraries | 18576B Mem Free
# board: 16 bytes
# digitalio: 0 bytes
# adafruit_dht: 3168 bytes
# time: 0 bytes
# adafruit_sdcard: 5744 bytes
# analogio: 0 bytes
# busio: 48 bytes
# storage: 0 bytes
# adafruit_ssd1306: 4784 bytes
